$(function () {

    ceshis();
    ceshis1();
    ceshis2();
    ceshis3();
    ceshis4();

    function ceshis() {
        var myChart = echarts.init(document.getElementById('chart1'));

        option = {
            /*backgroundColor: '#05163B',*/
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {
                        show: true
                    },
                    dataView: {
                        show: true,
                        readOnly: false
                    },
                    magicType: {
                        show: true,
                        type: ['line', 'bar']
                    },
                    restore: {
                        show: true
                    },
                    saveAsImage: {
                        show: true
                    }
                }
            },
            grid: {
                top: 'middle',
                left: '3%',
                right: '4%',
                bottom: '3%',
                top: '10%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                data: ['2015年1月', '2015年2月', '2015年3月', '2015年4月', '2015年5月', '2015年6月', '2015年7月', '2015年8月', '2015年9月', '2015年10月', '2015年11月', '2015年12月',
                    '2016年1月', '2016年2月', '2016年3月', '2016年4月', '2016年5月', '2016年6月', '2016年7月', '2016年8月', '2016年9月','2016年10月','2016年11月','2016年12月'
                ],
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: "#ebf8ac" //X轴文字颜色
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#01FCE3'
                    }
                },
            }],
            yAxis: [{
                type: 'value',
                name: '人数',
                axisLabel: {
                    formatter: '{value} /万人次',
                    textStyle: {
                        color: "#2EC7C9" //X轴文字颜色
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#01FCE3'
                    }
                },
            },
                {
                    type: 'value',
                    name: '人数',
                    axisLabel: {
                        formatter: '{value}/万人次',
                        textStyle: {
                            color: "#2EC7C9" //X轴文字颜色
                        }
                    },axisLine: {
                        lineStyle: {
                            color: '#01FCE3'
                        }
                    },
                }
            ],
            series: [

                {
                    name: '外国人',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            barBorderRadius: 5,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: "#00FFE3"
                            },
                                {
                                    offset: 1,
                                    color: "#4693EC"
                                }
                            ])
                        }
                    },
                    /*data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]*/
                    data: [35.9356, 25.5894, 48.4457, 49.8421, 50.6426, 44.8386, 44.7721,45.2538, 49.1983,54.8975, 47.8098, 43.4681,38.7108,34.1184,49.0636,51.8702,50.7590,46.3996,47.0448,48.5472,52.7624,60.2220,52.6742,46.9723],
                },
                {
                    name: '澳门 *1e-2',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            barBorderRadius: 5,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: "#C1B2EA"
                            },
                                {
                                    offset: 1,
                                    color: "#8362C6"
                                }
                            ])
                        }
                    },
                    data: [8.85,12.14, 16.44, 19.26, 18.21, 14.83, 15.71, 18.8, 12.32, 15.54, 16.38, 20.91,13.95,9.08,16.45,15.05,15.47,18.83,25.45,13.48,13.7,17.4,17.42,17.36],
                },
                {
                    name: '香港 *1e-1',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [31.576, 27.458, 40.413, 45.232, 42.024, 41.551, 35.704, 33.951, 35.953, 42.115, 42.109, 36.568,32.839,27.836,45.016,41.182,42.322,41.431,36.453,36.3,36.831,46.762,45.147,44.984],
                    lineStyle: {
                        normal: {
                            width: 5,
                            color: {
                                type: 'linear',

                                colorStops: [{
                                    offset: 0,
                                    color: '#AAF487' // 0% 处的颜色
                                },
                                    {
                                        offset: 0.4,
                                        color: '#47D8BE' // 100% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: '#47D8BE' // 100% 处的颜色
                                    }
                                ],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(71,216,190, 0.5)',
                            shadowBlur: 10,
                            shadowOffsetY: 7
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#AAF487',
                            borderWidth: 10,
                            /*shadowColor: 'rgba(72,216,191, 0.3)',
                             shadowBlur: 100,*/
                            borderColor: "#AAF487"
                        }
                    },
                    smooth: true,
                },
                {
                    name: '台湾 *1e-1',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [51.255, 32.098, 62.269, 59.697, 63.952,58.894,57.532,52.336,52.825,57.398,59.918,47.574,46.005,48.023,58.165,58.085,59.226,54.215,59.388,57.514,60.233,69.481,63.336,59.300],
                    lineStyle: {
                        normal: {
                            width: 5,
                            color: {
                                type: 'linear',

                                colorStops: [{
                                    offset: 0,
                                    color: '#F8B854' // 0% 处的颜色
                                },
                                    {
                                        offset: 0.4,
                                        color: '#DE801C' // 100% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: '#DE801C' // 100% 处的颜色
                                    }
                                ],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(71,216,190, 0.5)',
                            shadowBlur: 10,
                            shadowOffsetY: 7
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#F7AD3E',
                            borderWidth: 10,
                            /*shadowColor: 'rgba(72,216,191, 0.3)',
                             shadowBlur: 100,*/
                            borderColor: "#F7AD3E"
                        }
                    },
                    smooth: true,
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }




});