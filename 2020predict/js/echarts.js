$(function () {

    ceshis();
    ceshis1();
    ceshis2();
    ceshis3();
    ceshis4();

    function ceshis() {
        var myChart = echarts.init(document.getElementById('chart1'));

        option = {
            /*backgroundColor: '#05163B',*/
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {
                        show: true
                    },
                    dataView: {
                        show: true,
                        readOnly: false
                    },
                    magicType: {
                        show: true,
                        type: ['line', 'bar']
                    },
                    restore: {
                        show: true
                    },
                    saveAsImage: {
                        show: true
                    }
                }
            },
            grid: {
                top: 'middle',
                left: '3%',
                right: '4%',
                bottom: '3%',
                top: '10%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                data: ['2020年6月', '2020年7月', '2020年8月', '2020年9月', '2020年10月', '2020年11月', '2020年12月'
                ],
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: "#ebf8ac" //X轴文字颜色
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#01FCE3'
                    }
                },
            }],
            yAxis: [{
                type: 'value',
                name: '人数',
                axisLabel: {
                    formatter: '{value} /万人次',
                    textStyle: {
                        color: "#2EC7C9" //X轴文字颜色
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#01FCE3'
                    }
                },
            },
                {
                    type: 'value',
                    name: '人数',
                    axisLabel: {
                        formatter: '{value}/万人次',
                        textStyle: {
                            color: "#2EC7C9" //X轴文字颜色
                        }
                    },axisLine: {
                        lineStyle: {
                            color: '#01FCE3'
                        }
                    },
                }
            ],
            series: [

                {
                    name: '外国人',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            barBorderRadius: 5,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: "#00FFE3"
                            },
                                {
                                    offset: 1,
                                    color: "#4693EC"
                                }
                            ])
                        }
                    },
                    /*data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]*/
                    data: [3.5674,1.3470,0.1936,0.2691,0.3403,0.7040,4.0821],
                },
                {
                    name: '澳门 *1e-2',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            barBorderRadius: 5,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: "#C1B2EA"
                            },
                                {
                                    offset: 1,
                                    color: "#8362C6"
                                }
                            ])
                        }
                    },
                    data: [6.20,11.20,11.08,3.43,0.20,3.00,18.78],
                },
                {
                    name: '香港 *1e-1',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [9.561,15.245,14.159,3.406,0.446,4.717,19.003],
                    lineStyle: {
                        normal: {
                            width: 5,
                            color: {
                                type: 'linear',

                                colorStops: [{
                                    offset: 0,
                                    color: '#AAF487' // 0% 处的颜色
                                },
                                    {
                                        offset: 0.4,
                                        color: '#47D8BE' // 100% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: '#47D8BE' // 100% 处的颜色
                                    }
                                ],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(71,216,190, 0.5)',
                            shadowBlur: 10,
                            shadowOffsetY: 7
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#AAF487',
                            borderWidth: 10,
                            /*shadowColor: 'rgba(72,216,191, 0.3)',
                             shadowBlur: 100,*/
                            borderColor: "#AAF487"
                        }
                    },
                    smooth: true,
                },
                {
                    name: '台湾 *1e-1',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [19.246,11.365,1.681,4.838,6.328,2.390,7.821],
                    lineStyle: {
                        normal: {
                            width: 5,
                            color: {
                                type: 'linear',

                                colorStops: [{
                                    offset: 0,
                                    color: '#F8B854' // 0% 处的颜色
                                },
                                    {
                                        offset: 0.4,
                                        color: '#DE801C' // 100% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: '#DE801C' // 100% 处的颜色
                                    }
                                ],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(71,216,190, 0.5)',
                            shadowBlur: 10,
                            shadowOffsetY: 7
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#F7AD3E',
                            borderWidth: 10,
                            /*shadowColor: 'rgba(72,216,191, 0.3)',
                             shadowBlur: 100,*/
                            borderColor: "#F7AD3E"
                        }
                    },
                    smooth: true,
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }




});